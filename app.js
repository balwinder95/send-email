process.env.NODE_ENV = 'localDevelopment';
config = require('config');
constant = require('./routes/constant');

var emailSettings = require('./config/localDevelopment').emailSettings;
var port = require('./config/localDevelopment').PORT;
var path = require('path');
var commonfunction = require('./routes/commonfunction');
var routes = require('./routes/index');
var express = require('express');
var http = require('http');
var bodyParser = require('body-parser');
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.set('json spaces', 1);
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));

app.get('/test', routes);
app.post('/send_mail', routes);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

app.listen(port, function(){
    console.log("Working on port " + port);
});