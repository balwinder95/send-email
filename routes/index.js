var express = require('express');
var router = express.Router();
var commonfunction = require('./commonfunction');
/* GET test page. */
router.get('/test', function (req, res, next) {
  res.render('test');
});

router.post('/send_mail', commonfunction.sendEmail);

module.exports = router;