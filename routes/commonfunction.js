var emailSettings = require('../config/localDevelopment').emailSettings;
var sendResponse = require('./sendResponse');
var nodemailer = require('nodemailer');

exports.sendEmail = function(req, res) {
	var receiver = req.body.receiver;
	var smtpTransport = nodemailer.createTransport('SMTP', {
		service: "Gmail",
		auth: {
			user: emailSettings.email,
			pass: emailSettings.password
		}
	});

	var mailOptions = {
		from: "XYZ <" + emailSettings.email + ">",
		to: receiver,
		subject: "Hello",
		html: "<h3>Hello World!</h3>"
	}

	smtpTransport.sendMail(mailOptions, function (error, response) {
		if (receiver == '' || undefined) {
				sendResponse.parameterMissingError (res);
		} else if (error) {
			sendResponse.sendErrorMessage(constant.responseMessage.SHOW_ERROR_MESSAGE, res);
		}	else {
			console.log("Email sent: " + response.message);
			var data = {'to': receiver};
			sendResponse.sendSuccessData(data, res);
		}
	});
}